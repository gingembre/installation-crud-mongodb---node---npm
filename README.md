``Bonjour, 


Installation de node et de Npm : 

**sudo apt-get update
sudo apt-get install nodejs npm**

Installation mongoDB

1/Importez la clé publique utilisée par le système de gestion de paquets. 
Depuis un terminal, lancez la commande suivante pour importer la clé GPG publique MongoDB depuis https://www.mongodb.org/static/pgp/server-4.2.asc :

**wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -**

L'opération devrait répondre avec un OK.

2/Créez un fichier de liste pour MongoDB. 

Créez le fichier de liste /etc/apt/sources.list.d/mongodb-org-4.2.listpour votre version d'Ubuntu.

**echo  "deb [arch = amd64] https://repo.mongodb.org/apt/ubuntu bionic / mongodb-org / 4.2 multivers"  | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list**

3/ Recharger la base de données de paquets locale. 

Exécutez la commande suivante pour recharger la base de données du package local:

**sudo apt-get install -y mongodb-org**

Optionnel.
Bien que vous puissiez spécifier n'importe quelle version disponible de MongoDB, apt-getles packages seront mis à niveau lorsqu'une version plus récente sera disponible. 
Pour empêcher les mises à niveau inattendues, vous pouvez épingler le paquet dans la version actuellement installée:

**echo  "mongodb-org hold"  | sudo dpkg --set-selections**
**echo  "mongodb-org-server hold"  | sudo dpkg --set-selections**
**echo  "mongodb-org-shell hold"  | sudo dpkg --set-selections**
**echo  "mongodb-org-mongos hold"  | sudo dpkg --set-selections**
**echo  "mongodb-org-tools hold"  | sudo dpkg --set-selections**

 
 On va téster si l'installation est ok : 
 
 1/Démarrez MongoDB. 
Emettez la commande suivante pour démarrer mongod:

**sudo service mongod start**

2/Commencez à utiliser MongoDB. 

Démarrer un mongoshell sur la même machine hôte que le mongod.
Vous pouvez exécuter le mongoshell sans aucune option de ligne de commande pour vous connecter à un mongodordinateur en cours d'exécution 
sur votre hôte local avec le port par défaut 27017:

**mongo**

Maintenant le projet et les differentes bibliotheques :) 

1/On créer le dossier du projet

On fait à l'intérieur un :

**npm init**

Pour créer le package.json 

2/on installe les dépendances (express, mangoose, body-parser):

**npm install express --save**
**npm install mangoose --save**
**npm install body-parser --save**


ET VOILOU votre environnement de travail est ok :))))