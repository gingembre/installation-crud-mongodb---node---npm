require('dotenv').config();

// port=process.env.PORT || 3000

const express = require('express'),
      app = express(),
      mongoose = require('mongoose'),
      bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

let options = {
    user: process.env.MONGODB_USERNAME,
    pass: process.env.MONGODB_PASSWORD
};

mongoose.connect(process.env.DB_URI, options);
mongoose.set('debug', false);
//mongoose.connect('mongodb://127.0.0.1:27017/votreBaseDeDonnee');

// mon modele
let consoleSchema = mongoose.Schema({
    marque: String,
    annee: Number,
    nom: String
});

let Console = mongoose.model('Console', consoleSchema);

let router = express.Router();
router.route('/')
// recup l'ensembles des consoles
    .get(function (req, res) {
        Console.find(function (err, consoles) {
            if (err) {
                return res.send(err);
            }
            res.send(consoles);
        });
    })
    // envoi
    .post(function (req, res) {
        let gameStation = new Console();
        // instance de model
        gameStation.marque = req.body.marque;
        gameStation.annee = req.body.annee;
        gameStation.nom = req.body.nom;
        gameStation.save(function (err) {
            if (err) {
                return res.send(err);
            }
            res.send({message: 'console enregistré'});
        });
    });

router.route('/:console_id')
    .get(function (req, res) {
        Console.findOne({_id: req.params.console_id}, function (err, console) {
            if (err) {
                return res.send(err)
            }
            res.send(console);
        })
    })
    .put(function (req, res) {
        Console.findOne({_id: req.params.console_id}, function (err, console) {
            console.marque = req.body.marque;
            console.annee = req.body.annee;
            console.nom = req.body.nom;
            console.save(function (err) {
                if (err) {
                    return res.send(err);
                }
                res.send({message: 'mise a jour des consoles'});
            });
        });
    })
    .delete(function (req, res) {
        Console.remove({_id: req.params.console_id}, function (err) {
            if (err) {
                return res.send(err);
            }
            res.send({message: "Console effacée"});
        });
    });

app.use('/api', router);

/*app.lister(port, function(){
    console.log('listening on port ' + port);
})*/

app.listen(8050, function () {
    console.log('APP écoute serveur 8050');
})